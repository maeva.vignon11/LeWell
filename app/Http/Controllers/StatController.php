<?php

namespace App\Http\Controllers;

use App\Models\Stat;
use App\Models\Well;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatController extends Controller
{
    public function getByWell(Request $request, $well_id)
    { 
        if(Well::where('id', $well_id)->first() == null)
        {
            return response("L'id du puit n'existe pas ou n'a pas été trouvé", 404);
        }

        // mettre le parametre en timestamp ex : ?start_date=1546865111&end_date=1548938711
        if ($request->has('start_date') && $request->has('end_date')) {
            $startDate = Carbon::createFromTimestamp($request['start_date']);
            $endDate = Carbon::createFromTimestamp($request['end_date']);

            $stats = Stat::where('well_id', $well_id)
                ->where('date_stat', '>=', $startDate)
                ->where('date_stat', '<=', $endDate)
                ->get();

        } else {
            $stats = Stat::where('well_id', $well_id)->get();
        }

        return $stats;
    }

    public function create(Request $request)
    {
        $request->validate([
            'date_stat' => 'date|required',
            'level_water' => 'numeric|required',
            'is_consolidated' => 'boolean|required',
            'well_id' => 'numeric|required',
        ]);

        $current_well = Well::where('id', $request['well_id'])->first();

        if (!$current_well) {
            return response('Well not found', 404);
        }

        $new_stat = new Stat();
        $new_stat->date_stat = $request['date_stat'];
        $new_stat->level_water = $request['level_water'];
        $new_stat->is_consolidated = $request['is_consolidated'];
        $new_stat->well_id = $current_well->id;
        $new_stat->save();

        return response($new_stat, 201);
    }
}
