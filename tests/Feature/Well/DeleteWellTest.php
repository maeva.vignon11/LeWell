<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Well;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteWellTest extends TestCase
{
    use DatabaseTransactions; // Pour s'assurer que la base de données est rafraîchie avant chaque test
    use WithFaker;

    public function testDeleteWell()
    {
        $user = User::factory()->create();
        $well = Well::factory()->create(['user_id' => $user->id]);

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $response = $this->withHeaders($headers)->delete('/api/wells/'.$well->id);
        $response->assertStatus(200);

        $this->assertDatabaseMissing('wells', [
            'id' => $well->id,
        ]);
    }

    public function testDeleteWithMissingWell()
    {
        $user = User::factory()->create();

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $response = $this->withHeaders($headers)->delete('/api/wells/99999');
        $response->assertStatus(404);
    }

    public function testDeleteWellWithInvalidToken()
    {
        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();
        $well = Well::factory()->create(['user_id' => $user->id]);


        $response = $this->deleteJson('/api/wells/'.$well->id);

        $response->assertStatus(401)
                ->assertJson([
                    'message' => 'Unauthenticated.',
                ]);
    }
}
